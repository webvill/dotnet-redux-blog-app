using WebApi.Entities;

namespace WebApi.Models.Users;

public class AuthenticateResponse
{
    public User User { get; set; }
    public string Token { get; set; }
}