namespace WebApi.Models.Users;

using System.ComponentModel.DataAnnotations;

public class PostRequest
{
    [Required]
    public string Title { get; set; }

    [Required]
    public string Content { get; set; }
    [Required]
    public int UserId { get; set; }
}