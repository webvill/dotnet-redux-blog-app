namespace WebApi.Controllers;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Users;

[Authorize]
[ApiController]
[Route("[controller]")]
public class PostsController : ControllerBase
{
    private MySqlDataContext _context; 
    public PostsController(MySqlDataContext context)
    {
        _context = context;
    }
    [AllowAnonymous]
    [HttpGet]
    public async Task<IEnumerable<Post>> GetAll()
    {
        return await _context.Posts
            .Include(post => post.User)
            .Include(post => post.Comments)
            .ThenInclude(c => c.User)
            .ToListAsync();

    }
    [AllowAnonymous]
    [HttpGet("{id}")]
    public async Task<ActionResult<Post>> GetById(int id)
    {
            var post = await _context.Posts.FindAsync(id);
            if (post == null)
            {
                return NotFound();
            }
            return post;

    }

    [HttpPut("{id}")]
    public async Task<ActionResult<Post>> Update(PostRequest model, int id)
    {
            var post = await _context.Posts.FindAsync(id);
            if (post == null)
            {
                return NotFound();
            }
            post.Title = model.Title;
            post.Content = model.Content;
            post.UserId = model.UserId;
            
            _context.Posts.Update(post);
            await _context.SaveChangesAsync();
            
            return post;

    }

    [HttpPost]
    public async Task<ActionResult<Post>> Create(PostRequest model)
    {

            var ctxUser = (WebApi.Entities.User)HttpContext.Items.FirstOrDefault(item => (string)item.Key == "User").Value;
            
            
                
            if (ctxUser== null)
            {
                return BadRequest();
            }
            var post = new Post
            {
                Title = model.Title,
                Content = model.Content,
                UserId = ctxUser.Id,
                Date = DateTimeOffset.UtcNow

            };
            
            _context.Posts.Add(post);
            await _context.SaveChangesAsync();
            
            return post;

    }
}