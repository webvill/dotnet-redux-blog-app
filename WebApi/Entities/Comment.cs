using Newtonsoft.Json;

namespace WebApi.Entities;
public class Comment
{
    public int Id { get; set; }
    public int UserId { get; set; }
    public int PostId { get; set; }
    public string Content { get; set; }
    public DateTimeOffset Date { get; set; }
    [JsonIgnore]
    public Post Post { get; set; }
    public User User { get; set; }
    
}