namespace WebApi.Helpers;
using Microsoft.EntityFrameworkCore;
using WebApi.Entities;

public class MySqlDataContext : DbContext
{
    protected readonly IConfiguration Configuration;

    public MySqlDataContext(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        // connect to mysql server database
        var connectionString = Configuration.GetConnectionString("mySqlConnection"); 
        options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
    }

    public DbSet<User> Users { get; set; }
    public DbSet<Post> Posts { get; set; }
    public DbSet<Comment> Comments { get; set; }
}