import React from "react";
//import { useSelector } from "react-redux";

export const PostAuthor = ({ userId, author }) => {
   /* const author = useSelector((state) =>
      state.users.find((user) => user.id === userId)
   ); */

   return <span>by {author ? `${author.firstName} ${author.lastName}` : "Unknown author"}</span>;
};
