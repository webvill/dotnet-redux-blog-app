import { createSlice } from "@reduxjs/toolkit";
import { fetchPosts, updatePost, addPost } from "./postsThunk";
import { sub } from "date-fns";
/* const initialState = [
   {
      id: "1",
      userId: "1",
      title: "First Post!",
      content: "Hello!",
      date: sub(new Date(), { minutes: 5 }).toISOString(),
      reactions: { thumbsUp: 0, hooray: 0, heart: 0, rocket: 0, eyes: 0 },
   },
   {
      id: "2",
      userId: "2",
      title: "Second Post",
      content: "More text",
      date: sub(new Date(), { hours: 5 }).toISOString(),
      reactions: { thumbsUp: 0, hooray: 0, heart: 0, rocket: 0, eyes: 0 },
   },
]; */
const initialState = {status: "loading", posts: []}
export const postsSlice = createSlice({
   name: "posts",
   initialState,
   reducers: {
      
      reactionAdded(state, action) {
         const { postId, reaction } = action.payload;
         const existingPost = state.find((post) => post.id === postId);
         if (existingPost) {
            existingPost.reactions[reaction]++;
         }
      },
   },
   extraReducers: (builder) => {
      builder
         .addCase(fetchPosts.pending, (state, action) => {
            state.status = "loading";
         })
         .addCase(fetchPosts.fulfilled, (state, action) => {
            state.posts = action.payload;
            state.status = "idle";
         })
         .addCase(updatePost.pending, (state, action) => {
            state.status = "loading";
         })
         .addCase(updatePost.fulfilled, (state, action) => {
            state.posts = state.posts.map(post => post.id === action.payload.id ? action.payload : post);
            state.status = "idle";
         })
         .addCase(addPost.pending, (state, action) => {
            state.status = "loading";
         })
         .addCase(addPost.fulfilled, (state, action) => {
            state.posts.push(action.payload);
            state.status = "idle";
         });
   },
});

export const { reactionAdded } = postsSlice.actions;

export default postsSlice.reducer;
