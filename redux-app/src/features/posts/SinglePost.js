import React from "react";
import { useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { PostAuthor } from "./PostAuthor";
import { ReactionButtons } from "./ReactionButtons";
import { TimeAgo } from "./TimeAgo";

const SinglePost = () => {
   const { postId } = useParams();
   const post = useSelector((state) =>
      state.posts.posts.find((post) => post.id === parseInt(postId))
   );
   console.log("post", post);
   if (!post) {
      return (
         <section>
            <h2>Post not found!</h2>
         </section>
      );
   }

   return (
      <div className="container">
         <Link
            to={`/posts/${post.id}/edit`}
            className="btn btn-primary opacity-75"
         >
            Edit
         </Link>
         <article className="post">
            <h2>{post.title}</h2>
            <PostAuthor author={post.user} />
            <TimeAgo timestamp={post.date} />
            <p className="post-content">{post.content}</p>
            {/* <ReactionButtons post={post} /> */}
         </article>
      </div>
   );
};
export default SinglePost;
