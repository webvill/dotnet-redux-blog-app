import { createAsyncThunk } from "@reduxjs/toolkit";
import { getToken } from "../../utils/helpers";
import api from "../../services/api";
import { useParams } from "react-router-dom";

export const fetchPosts = createAsyncThunk(
   "posts/fetchPosts",
   async (_, { rejectWithValue }) => {
      try {
         const accessToken = getToken();
         api.defaults.headers.Authorization = `Bearer ${accessToken}`;
         const response = await api.get("/posts");
         return response.data;
      } catch (e) {
         return rejectWithValue("");
      }
   }
);

export const fetchPostById = createAsyncThunk(
   "posts/fetchPostById",
   async (_, { rejectWithValue }) => {
      try {
         const { postId } = useParams();
         const accessToken = getToken();
         api.defaults.headers.Authorization = `Bearer ${accessToken}`;
         const response = await api.get(`/posts/${postId}`);

         return response.data;
      } catch (e) {
         return rejectWithValue("");
      }
   }
);
export const updatePost = createAsyncThunk(
   "posts/updatePost",
   async (post, { rejectWithValue }) => {
      try {
         console.log("updateing", post);
         //const { postId } = useParams();

         const accessToken = getToken();
         api.defaults.headers.Authorization = `Bearer ${accessToken}`;
         const response = await api.put(`/posts/${parseInt(post.id)}`, {
            title: post.title,
            content: post.content,
            userId: post.userId,
         });
         return response.data;
      } catch (e) {
         return rejectWithValue("");
      }
   }
);


export const addPost = createAsyncThunk(
   "posts/addPost",
   async (post, { rejectWithValue }) => {
      try {
         const accessToken = getToken();
         api.defaults.headers.Authorization = `Bearer ${accessToken}`;
         const response = await api.post("/posts", { ...post });

         return response.data;
      } catch (e) {
         return rejectWithValue("");
      }
   }
);
