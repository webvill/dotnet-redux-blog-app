import { createSlice } from "@reduxjs/toolkit";
import { fetchUsers } from "./usersThunk";

const initialState = { status: "loading", users: [] };

export const usersSlice = createSlice({
   name: "users",
   initialState,
   reducers: {},
   extraReducers: (builder) => {
      builder
         .addCase(fetchUsers.pending, (state, action) => {
            state.status = "loading";
         })
         .addCase(fetchUsers.fulfilled, (state, action) => {
            state.users = action.payload;
            state.status = "idle";
         });
   },
});

export const { reactionAdded } = usersSlice.actions;

export default usersSlice.reducer;
