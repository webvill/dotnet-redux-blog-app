import { createAsyncThunk } from "@reduxjs/toolkit";
import { getToken } from "../../utils/helpers";
import api from "../../services/api";

export const fetchUsers = createAsyncThunk(
   "users/fetchUsers",
   async (_, { rejectWithValue }) => {
      try {
         const accessToken = getToken();
         api.defaults.headers.Authorization = `Bearer ${accessToken}`;
         const response = await api.get("/users");
         return response.data;
      } catch (e) {
         return rejectWithValue("");
      }
   }
);
