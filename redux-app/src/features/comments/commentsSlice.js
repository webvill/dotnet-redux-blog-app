import { createSlice } from "@reduxjs/toolkit";
const initialState = [
   { id: "1", post_id: 1, content: "Hello!" },
   { id: "2", post_id: 1, content: "More text" },
   { id: "2", post_id: 2, content: "First" },
];
export const commentsSlice = createSlice({
   name: "comments      ",
   initialState,
   reducers: {
      addComment(state, action) {
         state.push(action.payload);
      },
      updateComment(state, action) {
         const { id, title, content } = action.payload;
         const updatedState = state.map((p) =>
            p.id === id ? (p = action.payload) : p
         );
         state = updatedState;
      },
   },
});

export const { addComment, updateComment } = commentsSlice.actions;

export default commentsSlice.reducer;
