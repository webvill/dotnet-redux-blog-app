import { useState } from "react";
import { useSelector } from "react-redux";
import PostForm from "../features/posts/PostForm";
import PostsList from "../features/posts/PostsList";
const PostsPage = () => {
   const [showForm, setShowForm] = useState(false);
   const { token, userData } = useSelector((state) => state.auth);
   return (
      <div className="container">
         {token && (
            <>
               <button
                  onClick={() => setShowForm(!showForm)}
                  className="btn btn-primary opacity-75"
               >
                  {showForm ? "Dölj formulär" : "Lägg till post"}
               </button>
               {showForm && <PostForm />}
            </>
         )}
         <PostsList />
      </div>
   );
};

export default PostsPage;
