import { Navigate } from "react-router-dom";
import { useSelector } from "react-redux";

const ProtectedRoute = ({ children }) => {
    const { userData } = useSelector((state) => state.auth);
   if (!userData.firstName) {
      return <Navigate to="/" replace />;
   }

   return children;
};
export default ProtectedRoute;
