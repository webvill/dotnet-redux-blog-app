import React from "react";
import {useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { signOut } from "../features/users/authThunk";

const Home = () => {
   const { userData } = useSelector((state) => state.auth);

   const navigate = useNavigate();
   const dispatch = useDispatch();

   const handleSignout = () => {
      dispatch(signOut());
      navigate("/");
   };
   return (
      <div className="page">
         <h2>
            Home {userData.firstName} {userData.lastName}
         </h2>

         <div>
            <button onClick={handleSignout} className="btn btn-secondary">
               Logga ut
            </button>
         </div>
      </div>
   );
};

export default Home;
