import React from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import PostForm from "../features/posts/PostForm";

const EditPostPage = () => {
    const { postId } = useParams();
    const post = useSelector((state) =>
       state.posts.posts.find((post) => post.id === parseInt(postId))
    );

    if (!post) {
       return (
          <section>
             <h2>Post not found!</h2>
          </section>
       );
    }
   return <PostForm post={post} />;
};

export default EditPostPage;
