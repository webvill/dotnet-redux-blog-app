import React from "react";
import { Route, Routes } from "react-router-dom";
import Navbar from "./components/Navbar";
import SinglePost from "./features/posts/SinglePost";
import EditPostPage from "./routes/EditPostPage";
import Home from "./routes/Home";
import Login from "./routes/Login";
import PostsPage from "./routes/PostsPage";
import ProtectedRoute from "./routes/ProtectedRoute";

function App() {
   return (
      <>
         {/* <Navbar /> */}
         <Navbar />
         <Routes>
            <Route path="/" element={<PostsPage />} />
            <Route path="/posts/:postId" element={<SinglePost />} />
            <Route
               path="/posts/:postId/edit"
               element={
                  <ProtectedRoute>
                     <EditPostPage />
                  </ProtectedRoute>
               }
            />
            <Route path="/login" element={<Login />} />
            <Route
               path="home"
               element={
                  <ProtectedRoute>
                     <Home />
                  </ProtectedRoute>
               }
            />
         </Routes>
      </>
   );
}

export default App;
